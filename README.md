# bitset-benchmark

This is a template project based on CMake and the ability to clone github repositories as submodules. The goal of this project is to benchmark bitset and hashing libraries in C.

## CLion Quickstart

Open CLion and select Open Project, open the project folder.
Under Settings > Build, Execution, Deployment > Toolchains configure your toolchain

*  On Windows, use Visual Studio; move it up to the default
*  Set Environment > Architecture to a 64-bit target (e.g. amd64)

Then

*  Check for an antivirus notification; if you see one (Windows) select automatically configure to tell antivirus not to scan build dirs.
*  In the CMake window, click "Reload CMake Project".
*  Wait for the IDE to finish indexing, then select Run > Edit Configurations > <project name> > Before launch: Build, Activate tool window and click Add (+) and select Install.  Apply and OK.
*  From the Configuration dropdown, select <project name> and press Run.

## Command line CMake Quickstart

    cmake --build . --target install
