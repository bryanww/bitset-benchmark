
@echo off

setlocal
if not exist build mkdir build
cd build && call cmake ..
cmake --build . --config Release
cmake --install .
endlocal

